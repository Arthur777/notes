//
//  ListTableViewCell.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

final class ListTableViewCell: UITableViewCell {
    private enum Constants {
        // MARK: - Properties constants
        static let contentViewBackgroundColor = UIColor.white.cgColor
        static let contentViewCornerRadius: CGFloat = 20
        static let imageNormal = "backgroundWhite"
        static let imageSelected = "backgroundBlue"
        static let font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        static let colorBlack = UIColor.black
        static let color = UIColor(red: 249 / 255, green: 250 / 255, blue: 254 / 255, alpha: 100).cgColor
        static let colorGray = UIColor(red: 0.675, green: 0.675, blue: 0.675, alpha: 1)
        static let overallDimensions = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.medium)
        // MARK: - Constraint constants
        static let checkBoxOfsset: CGFloat = 35
        static let ofsset: CGFloat = 0
        static let littleOfsset: Double = 10
        static let middleOfsset: CGFloat = -30
        static let smallOfsset: CGFloat = -20
        static let bigOfsset: CGFloat = 40
    }
    // MARK: - Internal Properties
    var checkBoxClosure: ((Bool) -> Void)?
    // MARK: - Private Properties
    private var NSLayoutConstraint: NSLayoutConstraint?

    private var checkBox: UIButton = {
        let checkBox = UIButton()
        checkBox.settingImages(
            imageNormal: Constants.imageNormal,
            imageSelected: Constants.imageSelected
        )
        return checkBox
    }()

    private var note: UILabel = {
        let note = UILabel()
        note.settingFonts(fonts: Constants.font, color: Constants.colorBlack)
        return note
    }()

    private var message: UILabel = {
        let message = UILabel()
        message.settingFonts(fonts: Constants.overallDimensions, color: Constants.colorGray)
        return message
    }()

    private var time: UILabel = {
        let time = UILabel()
        time.settingFonts(fonts: Constants.overallDimensions, color: Constants.colorBlack)
        return time
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSettings()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - Private Function
    private func setupSettings() {
        contentView.layer.cornerRadius = Constants.contentViewCornerRadius
        contentView.layer.backgroundColor = Constants.contentViewBackgroundColor
        contentView.layer.borderColor = Constants.color
        contentView.layer.borderWidth = 3
        contentView.layer.shadowColor = UIColor.systemGray2.cgColor
        contentView.layer.shadowOffset = CGSize(width: 2, height: 2)
        contentView.layer.shadowRadius = 10
        contentView.layer.shadowOpacity = 2
        contentView.addSubviews([checkBox, note, message, time])

        checkBox.addTarget(self, action: #selector(toggleCheckboxSelection), for: .touchUpInside)
        checkBox.pinTop(to: contentView, Constants.checkBoxOfsset, .equal)

        for views in contentView.subviews {
            views.pinLeft(to: checkBox, Constants.bigOfsset, .equal)
        }

        note.pinTop(to: contentView, Constants.littleOfsset, .equal)
        message.pinBottom(to: note, Constants.middleOfsset, .equal)
        time.pinBottom(to: message, Constants.smallOfsset, .equal)

        NSLayoutConstraint = checkBox.trailingAnchor.constraint(
            equalTo: contentView.safeAreaLayoutGuide.leadingAnchor,
            constant: Constants.ofsset)
        NSLayoutConstraint?.isActive = true

    }
    func showCheckbox(_ show: Bool) {
        checkBox.isSelected = false
        UIView.animate(withDuration: 1) {
            self.NSLayoutConstraint?.constant = show ? 40 : 0
            self.layoutIfNeeded()
        }
    }

    func update(model: Note) {
        note.text = model.title
        message.text = model.text
        time.text = model.date
    }
    @objc func toggleCheckboxSelection() {
        checkBoxClosure?(!checkBox.isSelected)
        checkBox.isSelected = !checkBox.isSelected
    }
}
