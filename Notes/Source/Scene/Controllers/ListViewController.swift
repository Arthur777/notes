//
//  ListViewController.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

protocol ListViewControllerDataSource: AnyObject {
    func fetchDataView(id: UUID, time: String, message: String, title: String)
}

final class ListViewController: UIViewController,
                                ListViewControllerDataSource {
    private enum Constants {
        // MARK: - Properties constants
        static let navigationTitleText = "Заметки"
        static let dateFormat = "dd.MM.yyyy"
        static let plusButtonCleanImage = "Vector"
        static let plusButtonImage = "+"
        static let barButtonTitleSelect = "Выбрать"
        static let barButtonFetch = "Получить 🔖"
        static let barButtonTitleDone = "Готово"
        static let alertActionTitle = "Oк"
        static let alertControllerTitle = "Вы не выбрали ни одной заметки"
        static let deleteActionImage = "korzina"
        static let color = UIColor(red: 249 / 255, green: 250 / 255, blue: 254 / 255, alpha: 100)
        static let plusButtonCleanFont = UIFont.systemFont(ofSize: 37, weight: UIFont.Weight.regular)
        static let plusButtonCleanBackgroundcolor = UIColor(red: 0 / 255, green: 122 / 255, blue: 255 / 255, alpha: 1)
        static let plusButtonCornerRadius: CGFloat = 24
        static let plusButtonFont = UIFont.systemFont(ofSize: 37, weight: UIFont.Weight.regular)
        static let plusButtonBackgroundColor = UIColor(red: 0 / 255, green: 122 / 255, blue: 255 / 255, alpha: 1)
        static let plusButtonCleanCornerRadius: CGFloat = 24
        static let deleteActionBackgroundColor = UIColor(red: 249 / 255, green: 250 / 255, blue: 254 / 255, alpha: 100)
        // MARK: - Constraint constants
        static let bigOfsset: CGFloat = 50
        static let smallOfsset: CGFloat = 10
        static let bigHeight: Double = 860
        static let smallWeight: Double = 390
        static let buttonWightHeightAnchor: CGFloat = 50
    }
    // MARK: - Internal Properties
    let worker = Worker()
    var arrayWelcome = Welcome()
    // MARK: - Private Properties
    private var note = [Note]()
    private var saveNote = [Note]()
    private var indexPathSelect: [IndexPath] = []
    private let barRightButton = UIBarButtonItem()
    private let barLeftButton = UIBarButtonItem()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.tintColor = .blue
        tableView.sectionIndexBackgroundColor = UIColor.white
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = Constants.color
        return tableView
    }()

    private let plusButton: UIButton = {
        let plus = UIButton()
        plus.settingImages(imageNormal: Constants.plusButtonImage, imageSelected: "")
        plus.settingFonts(
            fonts: Constants.plusButtonFont,
            color: Constants.plusButtonBackgroundColor,
            radius: Constants.plusButtonCornerRadius
        )
        return plus
    }()

    private let cleanButton: UIButton = {
        let clean = UIButton()
        clean.settingImages(imageNormal: Constants.plusButtonCleanImage, imageSelected: "")
        clean.settingFonts(
            fonts: Constants.plusButtonCleanFont,
            color: Constants.plusButtonCleanBackgroundcolor,
            radius: Constants.plusButtonCleanCornerRadius
        )
        return clean
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = Constants.navigationTitleText
        view.backgroundColor = Constants.color
        view.addSubview(tableView)
        tableView.addSubviews([cleanButton, plusButton])

        setupConstraints()
        settingBarButtonItem()
        setupPlusButtonAnimatedUP()

        plusButton.addTarget(self, action: #selector(tapButton(_:)), for: .touchUpInside)
        cleanButton.addTarget(self, action: #selector(deleteRows(_:)), for: .touchUpInside)
        tableView.register(ListTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self

        let dateFormatter = DateFormatter()
        let timeInterval: CGFloat = 1652261857
        let date = Date(timeIntervalSince1970: timeInterval)
        dateFormatter.dateFormat = Constants.dateFormat
        let dateString = dateFormatter.string(from: date)

        self.note.append(Note(id: UUID(), title: UserService.note ?? .some("Заметка"), text: UserService.message ?? .some("Ура мы получили заметку"), date: UserService.time ?? dateString))

    }
    // MARK: - Private Function
    private func setupConstraints() {
        tableView.pinHorizontal(to: view)
        tableView.pinVertical(to: view)

        for views in tableView.subviews {
            views.pinRight(to: tableView, Constants.smallOfsset, .equal)
            views.setHeight(Constants.buttonWightHeightAnchor)
            views.setWidth(Constants.buttonWightHeightAnchor)
            views.pinBottom(to: tableView, Constants.bigOfsset, .equal)
        }
    }

    private func settingBarButtonItem() {
        barRightButton.title = Constants.barButtonTitleSelect
        barRightButton.target = self
        barRightButton.action = #selector(setupAnimationBarButtonTap)
        navigationItem.rightBarButtonItem = barRightButton

        barLeftButton.title = Constants.barButtonFetch
        barLeftButton.target = self
        barLeftButton.action = #selector(fetchData)
        navigationItem.leftBarButtonItem = barLeftButton
    }

    @objc private func fetchData() {
        worker.fetch { welcome in
            welcome.forEach { welcome in
                let timeInterval = TimeInterval(welcome.date)
                let dateFormatter = DateFormatter()
                let date = Date(timeIntervalSince1970: timeInterval)
                dateFormatter.dateFormat = Constants.dateFormat
                let dataString = dateFormatter.string(from: date)

                self.note.append(Note(id: UUID(), title: welcome.header, text: welcome.text, date: dataString))
            }
            DispatchQueue.main.sync {
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Private Function Animations
    private func setupPlusButtonTransitionLeft() {
        UIView().setupPlusButtonTransitionLeft(button: plusButton)
    }

    private func setupPlusButtonCleanTransitionLeft() {
        UIView().setupPlusButtonCleanTransitionLeft(
            cleanButton: cleanButton,
            tableView: tableView,
            image: Constants.plusButtonCleanImage
        )
    }

    private func setupPlusButtonTransitionRight() {
        UIView().setupPlusButtonTransitionRight(plusButton: plusButton, tableView: tableView, image: Constants.plusButtonImage)
    }

    private func setupPlusButtonCleanTransitionRight() {
        UIView().setupPlusButtonCleanTransitionRight(buttonClean: cleanButton)
    }

    @objc private func setupPlusButtonAnimatedUP() {
        UIView().setupPlusButtonAnimatedUP(plusButton: plusButton, cleanButton: cleanButton)
    }

    @objc private func setupPlusButtonAnimatedAddKeyFrame() {
        UIView().setupPlusButtonAnimatedAddKeyFrame(plusButton: plusButton, cleanButton: cleanButton)
    }

    @objc private func setupAnimationBarButtonTap() {
        if  barRightButton.title == Constants.barButtonTitleSelect {
            barRightButton.title = Constants.barButtonTitleDone
            barLeftButton.title = "Сохранить"
            barLeftButton.target = self
            barLeftButton.action = #selector(saveRows)
            setupPlusButtonTransitionLeft()
            setupPlusButtonCleanTransitionLeft()
            cellForEach(show: true)

        } else {
            barRightButton.title = Constants.barButtonTitleSelect
            barLeftButton.title = Constants.barButtonFetch
            barLeftButton.target = self
            barLeftButton.action = #selector(fetchData)
            if indexPathSelect.isEmpty {
                alert()
            } else {
                indexPathSelect.removeAll()
            }
            setupPlusButtonTransitionRight()
            setupPlusButtonCleanTransitionRight()

            cellForEach(show: false)
        }
    }

    // MARK: - Private Function Target
    private func alert() {
        let alertController = UIAlertController(
            title: Constants.alertControllerTitle,
            message: "",
            preferredStyle: .actionSheet
        )
        let action = UIAlertAction(title: Constants.alertActionTitle, style: .default) { [unowned self] (_) in
            barRightButton.title = Constants.barButtonTitleDone
            barLeftButton.title = "Сохранить"
            barLeftButton.target = self
            barLeftButton.action = #selector(saveRows)
            cellForEach(show: true)
            setupPlusButtonTransitionLeft()
            setupPlusButtonCleanTransitionLeft()
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    @objc func saveRows() {
            indexPathSelect.forEach {
                let listIndex = note[IndexPath(row: $0.row, section: 0).row]
                print("print: \(listIndex)")
                UserService.note = listIndex.title
                UserService.message = listIndex.text
                UserService.time = listIndex.date
        }
    }

    @objc private func deleteRows(_ cells: UIButton) {
        indexPathSelect.forEach {
            guard let getIndex = indexPathSelect.firstIndex(of: $0) else { return }
            indexPathSelect.remove(at: getIndex)

            barRightButton.title = Constants.barButtonTitleSelect
            tableView.isEditing = false
            setupPlusButtonTransitionRight()
            setupPlusButtonCleanTransitionRight()
            cellForEach(show: false)
            note.remove(at: getIndex)
            tableView.deleteRows(at: [IndexPath.init(row: getIndex, section: 0)], with: .fade)

        }
    }

    @objc private func tapButton(_ sender: UIButton) {
        let noteVC = NoteViewController()
        noteVC.delegateProtocol = self

        switch sender {
        case plusButton:
            UIView.animateKeyframes(
                withDuration: 1.2,
                delay: 0.1,
                options: [],
                animations: {
                    self.setupPlusButtonAnimatedAddKeyFrame()
                },
                completion: nil)

            DispatchQueue.global(qos: .userInteractive).async {
                sleep(UInt32(1.7))
                DispatchQueue.main.sync {
                    self.navigationController?.pushViewController(noteVC, animated: true)
                }
            }
        default:
            break
        }
    }
    // MARK: - Internal Function fetch Data
    func cellForEach(show: Bool) {
        (0...note.count).forEach {
            if let cell = tableView.cellForRow(at: IndexPath(item: $0, section: 0)) as? ListTableViewCell {
                print("print: \(show) \($0)")
                cell.showCheckbox(show)
            }
        }
    }

    func fetchDataView(id: UUID, time: String, message: String, title: String) {
        if let firstIndex = note.firstIndex(where: {$0.id == id}) {
            self.note[firstIndex].title = title
            self.note[firstIndex].text = message
            self.note[firstIndex].date = time
        } else {
            self.note.append(Note(id: id, title: title, text: message, date: time))
        }
        tableView.reloadData()
    }
}

extension ListViewController: UITableViewDelegate,
                              UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        note.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",
                                                 for: indexPath) as? ListTableViewCell
        guard let cell = cell else { return cell ?? .init(frame: .zero) }

        cell.backgroundConfiguration?.backgroundColor = Constants.color

        cell.update(model: note[indexPath.row])

        cell.checkBoxClosure = { select in
            if select {
                self.indexPathSelect.append(indexPath)
                print("print: select \(self.indexPathSelect)")
            } else {
                self.indexPathSelect = self.indexPathSelect.filter({ $0 != indexPath })
                print("print: deselect \(self.indexPathSelect)")
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let noteView = NoteViewController()
        noteView.delegateProtocol = self

        let index = note[indexPath.row]
        noteView.update(id: index.id,
                        note: index.title ?? "",
                        message: index.text ?? "",
                        data: index.date )

        self.indexPathSelect.removeAll()
        navigationController?.pushViewController(noteView, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        90
    }

    func tableView(
        _ tableView: UITableView,
        trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath
    ) -> UISwipeActionsConfiguration? {

        let deleteAction = UIContextualAction(style: .destructive, title: "") { _, _, _ in
            self.note.remove(at: indexPath.row)

            tableView.deleteRows(at: [indexPath], with: .fade)
        }

        deleteAction.image = UIImage(named: Constants.deleteActionImage)?.preparingForDisplay()
        deleteAction.backgroundColor = Constants.deleteActionBackgroundColor

        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = false

        return configuration
    }
}
