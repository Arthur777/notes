//
//  NoteViewController.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

final class NoteViewController: UIViewController {
    private enum Constants {
        // MARK: - Properties constants
        static let viewBackgroundColor = UIColor(red: 249 / 255, green: 250 / 255, blue: 254 / 255, alpha: 100)
        static let navigationButtonImage = "chevron.left"
        static let barbuttonText = "Готово"
        static let dateFormat = "dd.MM.yyyy EEEE HH:mm"
        static let dateForm = "dd.MM.yyyy"
        static let notePlacholder = "Введите название"
        static let noteLabelBackgroundColor = UIColor.black
        static let noteLabelFont = UIFont.systemFont(ofSize: 24.0, weight: UIFont.Weight.medium)
        static let textLabelColor = UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 1)
        static let textLabelFont = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        static let timeLabelFont = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        static let timeLabelColor = UIColor(red: 172 / 255, green: 172 / 255, blue: 172 / 255, alpha: 1)
        // MARK: - Constraint constants
        static let ofsset: CGFloat = 50
        static let smallHeight: CGFloat = 30
        static let smallOfsset: CGFloat = 20
        static let bigOfsset: CGFloat = 300
        static let bigHeight: CGFloat = 500
        static let bigWeight: CGFloat = 360
    }

    weak var delegateProtocol: ListViewControllerDataSource?

    @objc let barButton = UIBarButtonItem()

    private var identifire: UUID?

    private let label: UILabel = {
        let lab = UILabel()
        lab.isHidden = true
        lab.textColor = .black
        return lab
    }()

    private let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()

    private let notes: UITextField = {
        let textTitle = UITextField()
        textTitle.settingTextField(
            placolder: Constants.notePlacholder,
            color: Constants.noteLabelBackgroundColor,
            fonts: Constants.noteLabelFont
        )
        return textTitle
    }()

    private let text: UITextView = {
        let textView = UITextView()
        textView.settingsTextView(color: Constants.textLabelColor, fonts: Constants.textLabelFont)
        return textView
    }()

    private let time: UITextField = {
        let textTime = UITextField()
        textTime.settingTextField(
            placolder: "",
            color: Constants.timeLabelColor,
            fonts: Constants.timeLabelFont
        )
        textTime.textAlignment = .center
        textTime.isUserInteractionEnabled = false
        return textTime
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        view.backgroundColor = Constants.viewBackgroundColor

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: Constants.navigationButtonImage),
            style: .done,
            target: self,
            action: #selector(backToVC)
        )

        view.addSubview(scrollView)
        scrollView.addSubviews([time, notes, text])
        text.delegate = self
        notes.delegate = self
        text.adjustableForKeyboard()
        settingsConstraint()
        dateTextTitle()

        buttonRightSetting()
    }
    // MARK: - Private
    private func settingsConstraint() {
        scrollView.pin(to: view)
        time.pinTop(to: scrollView)
        time.pinLeft(to: scrollView, Constants.ofsset, .equal)
        time.setHeight(Constants.smallHeight)
        time.setWidth(Constants.bigOfsset)

        notes.pinTop(to: time, Constants.ofsset, .equal)
        notes.pinLeft(to: scrollView, Constants.smallOfsset, .equal)
        notes.setWidth(Constants.bigOfsset)

        text.pinTop(to: notes, Constants.ofsset, .equal)
        text.pinLeft(to: scrollView, Constants.smallOfsset, .equal)
        text.setHeight(Constants.bigHeight)
        text.setWidth(Constants.bigWeight)
    }

    private func buttonRightSetting() {
        barButton.title = Constants.barbuttonText
        barButton.isEnabled = false
        barButton.target = self
        barButton.action = #selector(endEdit)
        navigationItem.rightBarButtonItem = barButton
    }

    private func dateTextTitle() {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = Constants.dateFormat
        time.text = dateFormater.string(from: Date())
    }

    @objc private func endEdit() {
        if text.text != nil, notes.text != nil {
            barButton.title = ""
            view.endEditing(true)
        }
    }

    @objc private func backToVC(send: UIButton) {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = Constants.dateForm
        time.text = dateFormater.string(from: Date())

        delegateProtocol?.fetchDataView(
            id: identifire ?? UUID(),
            time: time.text ?? "",
            message: text.text ?? "",
            title: notes.text ?? ""
        )
        navigationController?.popViewController(animated: true)
    }
    // MARK: - Method update
    func update(id: UUID, note: String, message: String, data: String) {
        identifire = id
        notes.text = note
        text.text = message
        time.text = data
    }
}
