//
//  DefaultUserService.swift
//  Notes
//
//  Created by Arthur on 26.12.2022.
//

import UIKit


final class UserService {
    static var shared = UserService()
    enum ServiceKey: String {
        case note
        case messsage
        case time
   }

    static var note: String! {
        get {
            UserDefaults.standard.string(forKey: ServiceKey.note.rawValue)

        } set {
            let defaults = UserDefaults.standard
            let noteKey = ServiceKey.note.rawValue
            if let note = newValue {
                print("value: \(note) was added to key \(noteKey)")

                defaults.set(note, forKey: noteKey)
            } else {
                defaults.stringArray(forKey: noteKey)
            }
        }
    }

    static var message: String! {
        get {
            UserDefaults.standard.string(forKey: ServiceKey.messsage.rawValue)

        } set {
            let defaults = UserDefaults.standard
            let messageKey = ServiceKey.messsage.rawValue
            if let message = newValue {
                print("value: \(message) key: \(messageKey)")
                defaults.set(message, forKey: messageKey)
            } else {
                defaults.stringArray(forKey: messageKey)
            }
        }
    }

    static var time: String! {
        get {
            UserDefaults.standard.string(forKey: ServiceKey.time.rawValue)

        } set {
            let defaults = UserDefaults.standard
            let timeKey = ServiceKey.time.rawValue
            if let time = newValue {
                print("value: \(time) key: \(timeKey)")
                defaults.set(time, forKey: timeKey)
            } else {
                defaults.stringArray(forKey: timeKey)
            }
        }
    }
}
