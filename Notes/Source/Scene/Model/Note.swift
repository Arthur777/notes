//
//  Note.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

struct Note: Hashable {
    // MARK: - Internal properties
    var id = UUID()
    var title: String?
    var text: String?
    var date: String

    init(id: UUID, title: String?, text: String?, date: String) {
        self.id = id
        self.title = title
        self.text = text
        self.date = date
    }
}
