//
//  Extension+UITextView.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

extension UITextView {
    func settingsTextView(color: UIColor, fonts: UIFont) {
        textColor = color
        textAlignment = .left
        becomeFirstResponder()
        font = fonts
        translatesAutoresizingMaskIntoConstraints = false
    }
}
