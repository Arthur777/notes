//
//  Extension+UIButton.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

extension UIButton {
    func settingImages(imageNormal: String, imageSelected: String) {
       setImage(UIImage(named: imageNormal), for: .normal)
       setImage(UIImage(named: imageSelected), for: .selected)
       layer.masksToBounds = false
       clipsToBounds = true
       translatesAutoresizingMaskIntoConstraints = false
    }
    func settingFonts(fonts: UIFont, color: UIColor, radius: CGFloat) {
        titleLabel?.font = fonts
        layer.cornerRadius = radius
        backgroundColor = color
        translatesAutoresizingMaskIntoConstraints = false
    }
}
