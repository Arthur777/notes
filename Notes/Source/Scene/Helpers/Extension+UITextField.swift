//
//  Extension+UITextField.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

extension UITextField {
    func settingTextField(placolder: String, color: UIColor, fonts: UIFont) {
        placeholder = placolder
        textColor = color
        font = fonts
        textAlignment = .left
        translatesAutoresizingMaskIntoConstraints = false
    }
}
