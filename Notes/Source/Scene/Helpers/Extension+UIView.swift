//
//  Extension+UIView.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

//@available(iOS 4.0, *)
extension UIView {
   internal func addSubviews(_ views: [UIView]) {
        for view in views {
            addSubview(view)
        }
    }

   internal func setupPlusButtonTransitionLeft(button: UIButton) {
        UIView.transition(
            with: button,
            duration: 1,
            options: [.transitionFlipFromLeft],
            animations: {

            },
            completion: nil)
    }

    internal func setupPlusButtonCleanTransitionLeft(cleanButton: UIButton, tableView: UITableView, image: String) {
        UIView.transition(
            with: cleanButton,
            duration: 1,
            options: [.transitionFlipFromLeft],
            animations: {
                cleanButton.setImage(UIImage(named: image), for: .normal)
                tableView.addSubview(cleanButton)
            },
            completion: nil)
    }

   internal func setupPlusButtonTransitionRight(plusButton: UIButton, tableView: UITableView, image: String) {
        UIView.transition(
            with: plusButton,
            duration: 1,
            options: [.transitionFlipFromRight],
            animations: {
                plusButton.setImage(UIImage(named: image), for: .normal)
                tableView.addSubview(plusButton)

            },
            completion: nil)
    }

   internal func setupPlusButtonCleanTransitionRight(buttonClean: UIButton) {
        UIView.transition(
            with: buttonClean,
            duration: 1,
            options: [.transitionFlipFromRight],
            animations: {
            },
            completion: nil)
    }

   internal func setupPlusButtonAnimatedUP(plusButton: UIButton, cleanButton: UIButton) {
        UIView.animate(
            withDuration: 2.5,
            delay: 0,
            usingSpringWithDamping: 0.2,
            initialSpringVelocity: 0.1,
            options: [],
            animations: {
                plusButton.frame.origin.y -= 80
                cleanButton.frame.origin.y -= 80
            },
            completion: nil)
    }

    internal func setupPlusButtonAnimatedAddKeyFrame(plusButton: UIButton, cleanButton: UIButton) {
        UIView.addKeyframe(
            withRelativeStartTime: 0,
            relativeDuration: 0.25) {
                plusButton.frame.origin.y -= 30
                cleanButton.frame.origin.y -= 30
        }
        UIView.addKeyframe(
            withRelativeStartTime: 0.50,
            relativeDuration: 0.25) {
                plusButton.frame.origin.y += 60
                cleanButton.frame.origin.y += 60
        }
        UIView.addKeyframe(
            withRelativeStartTime: 0.8,
            relativeDuration: 0.25) {
                plusButton.frame.origin.y += 200
                cleanButton.frame.origin.y += 200
        }
    }
}
