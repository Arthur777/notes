//
//  Extension+UILabel.swift
//  Notes
//
//  Created by Arthur on 22.12.2022.
//

import UIKit

extension UILabel {
    func settingFonts(fonts: UIFont, color: UIColor) {
        font = fonts
        textColor = color
        textAlignment = .left
        textColor = color
        translatesAutoresizingMaskIntoConstraints = false
    }
}
